<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controller\RoutingController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// })->name('home');

// Route::get('/contact', function () {
//     return view('contact');
// })->name('contact');

// Route::get('/gallery', function () {
//     return view('gallery');
// })->name('gallery');

// Route::get('/visi', function () {
//     return view('visi');
// })->name('visi');

// Route::get('/profile', function () {
//     return view('profile');
// })->name('profile');

// Route::get('/goal', function () {
//     return view('goal');
// })->name('goal');

// Route::get('/about', function () {
//     return view('about');
// })->name('about');

// Route::get('/login', function () {
//     return view('auth/login');
// })->name('login');


// Route::get('/{page}', function () {
//     return view('home');
// })->name('home');

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/{page}', [App\Http\Controllers\RoutingController::class, 'index']);
