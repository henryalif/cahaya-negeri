
<ul>
    <li><a class="{{ Request::is('home') ?  'active' : ''}}"    href="/">Home</a></li>
    <li><a class="{{ Request::is('about') ?  'active' : ''}}"   href="about">About</a></li>
    <li><a class="{{ Request::is('visi') ?  'active' : ''}}"    href="visi">Vision & Mission</a></li>
    <li><a class="{{ Request::is('goal') ?  'active' : ''}}"    href="goal">Goals</a></li>
    <li><a class="{{ Request::is('profile') ?  'active' : ''}}" href="profile">Profile</a></li>
    <li><a class="{{ Request::is('gallery') ?  'active' : ''}}" href="gallery">Gallery</a></li>
    <li><a class="{{ Request::is('contact') ?  'active' : ''}}" href="contact">Contact</a></li>
</ul>
