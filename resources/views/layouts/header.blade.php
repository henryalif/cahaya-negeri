<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

    <h1 class="logo me-auto"><a href="/">PT.Cahaya Negeri Digital</a></h1>
    <nav id="navbar" class="navbar order-last order-lg-0">
        @include('layouts.menu')
        <i class="bi bi-list mobile-nav-toggle"></i>
    </nav><!-- .navbar -->

    <a href="login" class="get-started-btn">Login</a>

    </div>
</header>
<!-- End Header -->
