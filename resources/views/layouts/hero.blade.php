    <!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex justify-content-center align-items-center">
        <div class="container position-relative" data-aos="zoom-in" data-aos-delay="100">
            <h1>Selamat Datang Di<br>PT.Cahaya Negeri Digital</h1>
            <h2>Everyone can become an IT Expert</h2>
            <a href="#" class="btn-get-started">Get Started</a>
        </div>
    </section><!-- End Hero -->
