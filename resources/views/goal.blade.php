@extends('layouts.app')

@section('content')
@include('layouts.hero')

<main id="main">
    <!-- ======= Testimonials Section ======= -->
   <section id="tujuan" class="about">
        <div class="container" data-aos="fade-up">
            <h1 class="text-center mb-5">Our Goals</h1>
                <div class="row">
                    <div class="col-md-12" data-aos="fade-left" data-aos-delay="100">
                        <center>
                            <img src="assets/img/tujuan.jpg" style="width: 50%;" class="img-fluid" alt="">
                        </center>
                    </div>
                    <div class="col-md-12 content mt-3">
                    <div class="row">
                        <div class="col-md-6">
                            <ul>
                                <li><i class="bi bi-check-circle"></i> Turut melaksanakan dan menunjang kebijakan dan program Pemerintah di bidang Ekonomi dan Pembangunan
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li><i class="bi bi-check-circle"></i> Turut melaksanakan dan menunjang kebijakan dan program Pemerintah di bidang Elektronika.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    </section><!-- End About Section -->

</main><!-- End #main -->
@endsection
