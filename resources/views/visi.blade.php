@extends('layouts.app')

@section('content')
@include('layouts.hero')

<main id="main">
<section id="visimisi" class="about">
      <div class="container" data-aos="fade-up">

        <h1 class="text-center mb-5">Visi & Misi</h1>

        <div class="row">
          <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            <img src="assets/img/about.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
            <h3 class="text-center">Visi</h3>

            <ul>
              <li><i class="bi bi-check-circle"></i>Menjadi Perusahaan Teknologi Kelas Dunia yang Terpercaya’
              </li>

            </ul>
          </div>
        </div>

        <div class="row mt-5">
          <div class="col-md-6" data-aos="fade-right" data-aos-delay="100">
            <img src="assets/img/visi.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-md-6 order-2 order-lg-1 content">
            <h3 class="text-center">Misi</h3>
            <ul>
              <li><i class="bi bi-check-circle"></i>Kami perusahaan solusi total berbasis teknologi elektronika dan informasi.
              </li>
              <li><i class="bi bi-check-circle"></i> Kami memberikan produk dan layanan yang terkini dan berkelanjutan dengan menjamin keselamatan dan purna jual yang responsif.</li>
              <li><i class="bi bi-check-circle"></i> Kami berkontribusi menjaga kedaulatan negara dan meningkatkan kualitas hidup.
              </li>
            </ul>
          </div>

        </div>


      </div>
    </section><!-- End About Section -->
    </main><!-- End #main -->
@endsection
