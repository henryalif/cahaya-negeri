@extends('layouts.app')

@section('content')
@include('layouts.hero')

    <main id="main">

        <section id="why-us" class="why-us">
            <div class="container" data-aos="fade-up">

                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center">Title</h1>
                    </div>
                    <div class="col-lg-4 d-flex align-items-stretch">
                        <div class="content">
                            <h3>Why Choose Us?</h3>
                            <p>
                                Kami berpengalaman dalam dunia software dan keamanan jaringan sejak tahun 2000,dengan
                                sertifikasi yang kami miliki dan dengan beberapa team unggulan kami
                            </p>
                            <div class="text-center">
                                <a href="about.html" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                        <div class="icon-boxes d-flex flex-column justify-content-center">
                            <div class="row">
                                <div class="col-xl-4 d-flex align-items-stretch">
                                    <div class="icon-box mt-4 mt-xl-0">
                                        <i class="bx bx-receipt"></i>
                                        <h4>Trust</h4>
                                        <p>Kami telah dipercayai oleh client kami dari seluruh dunia </p>
                                    </div>
                                </div>
                                <div class="col-xl-4 d-flex align-items-stretch">
                                    <div class="icon-box mt-4 mt-xl-0">
                                        <i class="bx bx-cube-alt"></i>
                                        <h4>Clean</h4>
                                        <p>Code yang kami lakukan dapat di baca kembali sesuai ketentuan developer dunia
                                        </p>
                                    </div>
                                </div>
                                <div class="col-xl-4 d-flex align-items-stretch">
                                    <div class="icon-box mt-4 mt-xl-0">
                                        <i class="bx bx-images"></i>
                                        <h4>Secure</h4>
                                        <p>Jaminan dalam sistem bisa diuji keamanan nya ,telah dilakukan uji ketahanan
                                            dengan team security kami</p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End .content-->
                    </div>
                </div>

            </div>
        </section><!-- End Why Us Section -->
        <!-- ======= Map Section ======= -->
        <section id="about" class="about">
            <div class="container" data-aos="fade-up">

                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-center">PT.Cahaya Negeri Digital</h3>
                    </div>
                    <div class="col-md-12">
                        <div id="map" style="height:500px; width: 100%;" class="my-3 map"></div>
                    </div>
                </div>

            </div>
        </section><!-- End Map Section -->
    </main>
@endsection
