@extends('layouts.app')

@section('content')
<main id="main">
    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
      <div class="container">
        <h2>Galery</h2>
        <p>Berikut ini adalah Galery Produk yang kami buat atau kerjakan </p>
      </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Events Section ======= -->
   <section id="gallery" class="events">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/events-1.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Teknologi Informasi, Komunikasi & Sistem Navigasi</a></h5>
                <p class="fst-italic text-center">Sunday, September 26th at 7:00 pm</p>
                <p class="card-text">PT Cahaya Negri Digital (Persero) memiliki pengalaman dalam bidang telekomunikasi selama puluhan tahun. Len Industri sukses menyediakan berbagai macam solusi untuk kebutuhan pelanggan dalam bidang Teknologi Informasi, Komunikasi dan Sistem Navigasi.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/events-2.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Teknologi Elektronika Pertahanan</a></h5>
                <p class="fst-italic text-center">Sunday, November 15th at 7:00 pm</p>
                <p class="card-text">PT Cahaya Negri Digital (Persero) merupakan BUMN Industri Pertahanan yang fokus di bidang C4ISR (Command, Control, Communication, Computer, Intelligence, Surveillance and Reconnaissance). Len Industri melakukan pengembangan mandiri produk pertahanan maupun melalui kerja sama sebagai mitra strategis untuk memenuhi kebutuhan alat utama sistem senjata (alutsista) militer Indonesia.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/it.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Teknologi Sistem Transportasi</a></h5>
                <p class="fst-italic text-center">Sunday, September 26th at 7:00 pm</p>
                <p class="card-text">PT Cahay Negri Digital (Persero) merupakan pemain utama dan satu-satunya industri persinyalan kereta api di Indonesia, serta telah memulai bidang ini sejak lebih dari 35 tahun lalu. Len Industri menempatkan keamanan dan keandalan sebagai hal utama dalam pengembangan produk dengan motto “failsafe – no compromise“.</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch">
            <div class="card">
              <div class="card-img">
                <img src="assets/img/galery.jpg" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="">Teknologi Energi Terbarukan</a></h5>
                <p class="fst-italic text-center">Sunday, November 15th at 7:00 pm</p>
                <p class="card-text">Sistem Pembangkit Listrik Tenaga Surya (PLTS) merupakan salah satu sumber energi alternatif yang sangat tepat digunakan di Indonesia serta dapat mengurangi ketergantungan terhadap bahan bakar minyak. Len Industri telah merintis bisnis PLTS sejak tahun 1985 dan telah diaplikasikan di berbagai daerah di Indonesia.</p>
              </div>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End Events Section -->

  </main><!-- End #main -->
@endsection