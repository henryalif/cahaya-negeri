@extends('layouts.app')

@section('content')
@include('layouts.hero')

<main id="main">
        <section id="why-us" class="why-us">
            <div class="container" data-aos="fade-up">

                <div class="row">
                    <div class="col-lg-12 d-flex align-items-stretch">
                        <div class="content">
                            <h3>About</h3>
                            <p>
                                Perusahaan kami bergerak dibidang pelayanan jasa pembuatan sistem informasi dan keamanan security jaringan
                            </p>
                            <div class="text-center">
                                <a href="about.html" class="more-btn">Learn More <i class="bx bx-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End Why Us Section -->

    </main><!-- End #main -->

@endsection
